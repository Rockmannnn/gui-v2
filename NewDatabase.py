from ImportData import *
from tkinter import *

class NewDatabase:

    def __init__(self, window, height, width, databaseurl, password, eegandann, personality, detectedemotions,
                 parametertable):
        self.window = window
        self.height = height
        self.width = width
        self.databaseurl = databaseurl
        self.password = password
        self.eegandann = eegandann
        self.personality = personality
        self.detectedemotions = detectedemotions
        self.parametertable = parametertable

    def getwindow(self):
        return self.window
    def getheight(self):
        return self.height
    def getwidth(self):
        return self.width
    def getdatabaseurl(self):
        return self.databaseurl
    def getpassword(self):
        return self.password
    def geteegandann(self):
        return self.eegandann
    def getpersonality(self):
        return self.personality
    def getdetectedemotions(self):
        return self.detectedemotions
    def getparametertable(self):
        return self.parametertable

    def setwindow(self, window):
        self.window = window
    def setheight(self, height):
        self.height = height
    def setwidth(self, width):
        self.width = width
    def setdatabaseurl(self, databaseurl):
        self.databaseurl = databaseurl
    def setpassword(self, password):
        self.password = password
    def seteegandann(self, eegandann):
        self.eegandann = eegandann
    def setpersonality(self, personality):
        self.personality = personality
    def setdetectedemotions(self, detectedemotions):
        self.detectedemotions = detectedemotions
    def setparametertable(self, parametertable):
        self.parametertable = parametertable

    def makeWindow(self):
        self.getwindow().title("NEW DATABASE")
        self.getwindow().geometry(str(self.getheight()) + "x" + str(self.getwidth()))

        Label(self.getwindow(),text="EEG frequency").place(x=25, y=25)
        Label(self.getwindow(), text="Epoch length").place(x=25, y=50)
        Label(self.getwindow(), text="Window size").place(x=25, y=75)
        Label(self.getwindow(), text="Window overlap").place(x=25, y=100)
        Label(self.getwindow(), text="Correlation threshold").place(x=25, y=125)

        number1 = StringVar()
        number2 = StringVar()
        number3 = StringVar()
        number4 = StringVar()
        number5 = StringVar()

        number1.set(self.getparametertable()[0])
        number2.set(self.getparametertable()[1])
        number3.set(self.getparametertable()[2])
        number4.set(self.getparametertable()[3])
        number5.set(self.getparametertable()[4])

        entryval1 = Entry(self.getwindow(), textvariable=number1)
        entryval2 = Entry(self.getwindow(), textvariable=number2)
        entryval3 = Entry(self.getwindow(), textvariable=number3)
        entryval4 = Entry(self.getwindow(), textvariable=number4)
        entryval5 = Entry(self.getwindow(), textvariable=number5)

        entryval1.place(x = 150,y = 25)
        entryval2.place(x = 150,y = 50)
        entryval3.place(x=150, y=75)
        entryval4.place(x=150, y=100)
        entryval5.place(x=150, y=125)

        tabllica = [entryval1,entryval2,entryval3,entryval4,entryval5]

        newdatabasebutton = Button(self.getwindow(), width=10, height=1, text="Continue",command = lambda: self.importData(tabllica))

        newdatabasebutton.place(x = 125 ,y  = 160)

        self.getwindow().mainloop()

    def importData(self,tabllica):
        print("IMPROTING DATA")

        tablicapomocnicza = [0.0,0.0,0.0,0.0,0.0]

        for i in range(5):
            tablicapomocnicza[i] = tabllica[i].get()

        self.setparametertable(tablicapomocnicza)

        print(self.getparametertable())

        f = open("Pliczek.txt", "w+")
        f.write(self.getdatabaseurl() + "\n" +
                self.getpassword() + "\n" +
                self.geteegandann() + "\n" +
                self.getpersonality() + "\n" +
                self.getdetectedemotions() + "\n" +
                str(self.getparametertable()[0]) + "\n" +
                str(self.getparametertable()[1]) + "\n" +
                str(self.getparametertable()[2]) + "\n" +
                str(self.getparametertable()[3]) + "\n" +
                str(self.getparametertable()[4])
                )
        f.close()

        self.getwindow().destroy()

        #self.getwindow().destroy()

        imporujdane = ImportData(Tk(), self.getheight(), self.getwidth(), self.getdatabaseurl(), self.getpassword(),
                                 self.geteegandann(), self.getpersonality(), self.getdetectedemotions(),self.getparametertable())
        imporujdane.makeWindow()
