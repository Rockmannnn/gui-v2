from neo4j import GraphDatabase
from amigos import get_preprocessed
from data import *

# TODO: currently assumes frquency of 128Hz, using a variable might be a good idea (could have a 128Hz default)


def process_ext_annotations(driver, data, user_id, video_id):
    """Processes external annotation data and imports it into neo4j.
    :param data: a pandas DataFrame containing external annotations
    :param user_id: integer uniquely identifying a user
    :param video_id: string uniquely identifying a video
    :return:
    """

    # if not exists: create Video, User and MetaNodes for Time and External_Annotations
    #query = f"CREATE (data_ext: MetaNode {{name: 'External Annotations', user_id:{user_id},video_id:{video_id} }})\n"
    query = f"MATCH (s:Situation{{type:'Watching a movie', user_id:{user_id}, video_id:{video_id}}}) MERGE (data_ext: MetaNode {{name: 'External Annotations', user_id:{user_id},video_id:{video_id} }})<-[:HAS]-(s)\n"

    for q in data:
        signal_id = int(q[0])
        query += f"CREATE (EA_Valence{signal_id}: Signal {{type:'Valence', value:'{q[1]}'}})\n"
        query += f"CREATE (EA_Arousal{signal_id}: Signal {{type:'Arousal', value:'{q[2]}'}})\n"

    n1 = 1
    n2 = 2
    while n2 <= len(data):
        query += f"MERGE (EA_Valence{n1})-[:NEXT]->(EA_Valence{n2})\n"
        query += f"MERGE (EA_Arousal{n1})-[:NEXT]->(EA_Arousal{n2})\n"
        n1 += 1
        n2 += 1

    # creating metanodes
    query += "MERGE (data_ext)-[:HAS]->(data_valence: MetaNode { type: 'ValenceSignal'})\n"
    query += "MERGE (data_ext)-[:HAS]->(data_arousal: MetaNode { type: 'ArousalSignal'})\n"

    for q in data:
        signal_id = int(q[0])
        query += f"MERGE (EA_Valence{signal_id})<-[:HAS]-(data_valence)\n"
        query += f"MERGE (EA_Arousal{signal_id})<-[:HAS]-(data_arousal)\n"


    session = driver.session()
    session.run(query)
    session.close()


def remove_ext_annotations():
    """Removes all External Annotations Source Nodes from neo4j database"""

    driver = GraphDatabase.driver("bolt://127.0.0.1:7687", auth=("neo4j", neo4j_pass), encrypted=False)
    query = "MATCH (m:MetaNode{name:'External Annotations'})-[*0..]->(x) DETACH DELETE m,x"

    session = driver.session()
    session.run(query)
    session.close()


if __name__ == "__main__":
    #remove_ext_annotations()
    data_preprocessed = get_preprocessed(video_number=1, user_id=1)
    video_id = data_preprocessed.get("video_id")
    process_ext_annotations(data=data_preprocessed.get("ext_annotation"), user_id=1, video_id=video_id)
