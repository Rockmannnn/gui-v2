from amigos import get_preprocessed
from amigos import get_personality
from amigos import ELECTRODES
from SSP_neo4j import process_voltmat
from ExternalAnnotationsProcessing.CreateSourceExtAnnot import process_ext_annotations
from SSP_neo4j import create_personality_node
from SSP_neo4j import process_metanodes_time
from SSP_neo4j import get_timestamps
from neo4j import GraphDatabase
from data import *

def zrobgraf():

    driver = GraphDatabase.driver(databaseurl, auth=("neo4j", neo4j_pass), encrypted=False)

    # Get data
    data_preprocessed = get_preprocessed(video_number=video_number, user_id=user_id)
    data_personality = get_personality()
    video_id = data_preprocessed.get("video_id")
    time = data_preprocessed.get("time")

    # Process preprocessed data
    process_metanodes_time(driver=driver, video_length=time, epoch_length=20, user_id=user_id, video_id=video_id)
    process_ext_annotations(driver=driver, data=data_preprocessed.get("ext_annotation"), user_id=user_id, video_id=video_id)
    process_voltmat(driver=driver, data=data_preprocessed.get("voltmat"), electrodes=ELECTRODES, user_id=user_id, video_id=video_id)

    # Process personality data
    create_personality_node(driver=driver, data=data_personality, user_id=user_id)


