
f=open("Pliczek.txt", "r")

if f.mode == 'r':
    contents = f.read()
    a = contents.split("\n")

f.close()

databaseurl = a[0]
password = a[1]
eegandann = a[2]
personality = a[3]
detectedemotions = a[4]
paramtertable = [a[5],a[6],a[7],a[8],a[9]]

ELECTRODES = ['AF3', 'F7', 'F3', 'FC5', 'T7', 'P7', 'O1', 'O2', 'P8', 'T8', 'FC6', 'F4', 'F8', 'AF4']

neo4j_pass = password  # password to neo4j database
PATH_PREPROCESSED = eegandann
PATH_METADATA = personality
FREQUENCY = int(float(a[5]))  # default frequency of 128 Hz
EPOCH = int(float(a[6])) * FREQUENCY  # default epoch length, 20 seconds
WINDOW = 1 * FREQUENCY  # default window width, 1 second
OVERLAP = round(float(a[8]) * float(a[7]))  # default window overlap, 40% of window width
THRESHOLD = float(a[9])  # default correlation threshold

SOURCE_EPOCH = EPOCH * FREQUENCY  # 20 seconds at 128 Hz

user_id = 1
video_number = 1