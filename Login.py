from functools import partial
from tkinter import *

from Start import *

class Login:

    def __init__(self,window,height,width,databaseurl,password,eegandann,personality,detectedemotions,parametertable):
        self.window = window
        self.height = height
        self.width = width
        self.databaseurl = databaseurl
        self.password = password
        self.eegandann = eegandann
        self.personality = personality
        self.detectedemotions = detectedemotions
        self.parametertable = parametertable

    def getwindow(self):
        return self.window
    def getheight(self):
        return self.height
    def getwidth(self):
        return self.width
    def getdatabaseurl(self):
        return self.databaseurl
    def getpassword(self):
        return self.password
    def geteegandann(self):
        return self.eegandann
    def getpersonality(self):
        return self.personality
    def getdetectedemotions(self):
        return self.detectedemotions
    def getparametertable(self):
        return self.parametertable

    def setwindow(self,window):
        self.window = window
    def setheight(self,height):
        self.height = height
    def setwidth(self,width):
        self.width = width
    def setdatabaseurl(self,databaseurl):
        self.databaseurl = databaseurl
    def setpassword(self,password):
        self.password = password
    def seteegandann(self, eegandann):
        self.eegandann = eegandann
    def setpersonality(self, personality):
        self.personality = personality
    def setdetectedemotions(self, detectedemotions):
        self.detectedemotions = detectedemotions
    def setparametertable(self, parametertable):
        self.parametertable = parametertable

    def makewindow(self):

        self.window.title("LOGIN")
        self.window.geometry(str(self.getheight()) + "x" + str(self.getwidth()))

        number1 = StringVar()
        number2 = StringVar()

        number1.set(self.getdatabaseurl())
        number2.set(self.getpassword())

        Label(self.getwindow(), text="Database URL ").grid(row=1, column=0)
        Label(self.getwindow(), text="Password ").grid(row=2, column=0)

        entryval1 = Entry(self.getwindow(), textvariable=number1)
        entryval2 = Entry(self.getwindow(), textvariable=number2)

        entryval1.grid(row=1, column=2)
        entryval2.grid(row=2, column=2)

        # creating a button instance
        continuebutton = Button(self.getwindow(), width=10, height=2, text="Continue", command = lambda: self.saveDataAndContinue(entryval1,entryval2))

        # placing the button on my window
        continuebutton.place(x=150, y=120)

        self.window.mainloop()

    def saveDataAndContinue(self,databaseurl,password):

        if password.get():

            self.setdatabaseurl(databaseurl.get())
            self.setpassword(password.get())

            print("SAVED ",self.getdatabaseurl(),"   ",self.getpassword())

            self.getwindow().destroy()

            f = open("Pliczek.txt", "w+")
            f.write(self.getdatabaseurl()+"\n"+
                    self.getpassword() + "\n" +
                    self.geteegandann() + "\n" +
                    self.getpersonality() + "\n" +
                    self.getdetectedemotions() + "\n" +
                    str(self.getparametertable()[0]) + "\n" +
                    str(self.getparametertable()[1]) + "\n" +
                    str(self.getparametertable()[2]) + "\n" +
                    str(self.getparametertable()[3]) + "\n" +
                    str(self.getparametertable()[4])
                    )
            f.close()

            startoweokno = Start(Tk(),self.getheight(),self.getwidth(),self.getdatabaseurl(),self.getpassword(),self.geteegandann(),self.getpersonality(),self.getdetectedemotions(),self.getparametertable())
            startoweokno.makeWindow()
        else:
            print("NIE PODALES HASLA")
