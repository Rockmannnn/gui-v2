from tkinter import *
from tkinter import filedialog

class ImportData:

    def __init__(self, window, height, width, databaseurl, password,eegandann,personality,detectedemotions,parametertable):
        self.window = window
        self.height = height
        self.width = width
        self.databaseurl = databaseurl
        self.password = password
        self.eegandann = eegandann
        self.personality = personality
        self.detectedemotions = detectedemotions
        self.personality = personality
        self.detectedemotions = detectedemotions
        self.parametertable = parametertable

    def getwindow(self):
        return self.window
    def getheight(self):
        return self.height
    def getwidth(self):
        return self.width
    def getdatabaseurl(self):
        return self.databaseurl
    def getpassword(self):
        return self.password
    def geteegandann(self):
        return self.eegandann
    def getpersonality(self):
        return self.personality
    def getdetectedemotions(self):
        return self.detectedemotions
    def getpersonality(self):
        return self.personality
    def getdetectedemotions(self):
        return self.detectedemotions
    def getparametertable(self):
        return self.parametertable

    def setwindow(self, window):
        self.window = window
    def setheight(self, height):
        self.height = height
    def setwidth(self, width):
        self.width = width
    def setdatabaseurl(self, databaseurl):
        self.databaseurl = databaseurl
    def setpassword(self, password):
        self.password = password
    def seteegandann(self,eegandann):
        self.eegandann = eegandann
    def setpersonality(self,personality):
        self.personality = personality
    def setdetectedemotions(self,detectedemotions):
        self.detectedemotions = detectedemotions
    def setpersonality(self, personality):
        self.personality = personality
    def setdetectedemotions(self, detectedemotions):
        self.detectedemotions = detectedemotions
    def setparametertable(self, parametertable):
        self.parametertable = parametertable

    def makeWindow(self):

        self.getwindow().title("START")
        self.getwindow().geometry(str(self.getheight()) + "x" + str(self.getwidth()))

        number1 = StringVar()
        number2 = StringVar()
        number3 = StringVar()

        number1.set(self.geteegandann())
        number2.set(self.getpersonality())
        number3.set(self.getdetectedemotions())

        # creating a button instance
        datapreprocessedbutton = Button(self.getwindow(), width=14, height=1, text="EEG & Annotations ",command=lambda: self.choose_file(number1))
        passwordbutton = Button(self.getwindow(), width=14, height=1, text="Personality ",command=lambda: self.choose_file(number2))
        eegandannotationsbutton = Button(self.getwindow(), width=14, height=1, text="Detected Emotions ",command=lambda: self.choose_file(number3))

        datapreprocessedbutton.grid(row = 1,column = 0)
        passwordbutton.grid(row = 2,column = 0)
        eegandannotationsbutton.grid(row = 3,column = 0)

        entryval1 = Entry(self.getwindow(), textvariable=number1)
        entryval2 = Entry(self.getwindow(), textvariable=number2)
        entryval3 = Entry(self.getwindow(), textvariable=number3)

        entryval1.grid(row=1, column=6)
        entryval2.grid(row=2, column=6)
        entryval3.grid(row=3, column=6)

        uploadbutton = Button(self.getwindow(), width=8, height=2, text="Upload",command=lambda: self.saveData(number1,number2,number3))
        uploadbutton.place(x = 150,y = 150)

    def choose_file(self,filename):

        filename.set(filedialog.askopenfilename())

    def saveData(self,eegandann,personality,detectedemotions):

        self.seteegandann(eegandann.get())
        self.setpersonality(personality.get())
        self.setdetectedemotions(detectedemotions.get())


        f = open("Pliczek.txt", "w+")
        f.write(self.getdatabaseurl() + "\n" +
                self.getpassword() + "\n" +
                self.geteegandann() + "\n" +
                self.getpersonality() + "\n" +
                self.getdetectedemotions() + "\n" +
                str(self.getparametertable()[0]) + "\n" +
                str(self.getparametertable()[1]) + "\n" +
                str(self.getparametertable()[2]) + "\n" +
                str(self.getparametertable()[3]) + "\n" +
                str(self.getparametertable()[4])
                )
        f.close()

        self.getwindow().destroy()


        #self.getwindow().destroy()

        print("SAVED ")
        print("frequency ",self.getparametertable()[0])
        print("epoch length ",self.getparametertable()[1])
        print("window size ",self.getparametertable()[2])
        print("window overlap ",self.getparametertable()[3])
        print("correlation threshold ",self.getparametertable()[4])
        print("database url ",self.getdatabaseurl())
        print("password ",self.getpassword())
        print("eeq & annotations ",self.geteegandann())
        print("personality ",self.getpersonality())
        print("detected emotions ",self.getdetectedemotions())
