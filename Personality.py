#import pandas as pd
#from neo4j import GraphDatabase
from data import *

def create_personality_node(entryfordatatable,entryforpathtable,session):

    personalitydata = pd.ExcelFile(str(entryforpathtable[2]))

    df = personalitydata.parse("Personalities")

    user_id = int(entryfordatatable[1])

    query = "MATCH (a:User {id: "+str(user_id)+"}) MERGE (:Personality{UserID: '"+str(user_id)+"', Extrovertion: '"+str(df[user_id][0])+"', Agreeableness:'"+str(df[user_id][1])+"', Conscientiousness:'"+str(df[user_id][2])+"', Emotional_Stability: '"+str(df[user_id][3])+"', Creativity: '"+str(df[user_id][4])+"'})<-[r:HAS]-(a)"
    print(query)
    session.run(query)

    session.close()
