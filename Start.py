from ImportData import *
from Confirmation import *
from tkinter import *

class Start:

    def __init__(self,window,height,width,databaseurl,password,eegandann,personality,detectedemotions,parametertable):
        self.window = window
        self.height = height
        self.width = width
        self.databaseurl = databaseurl
        self.password = password
        self.eegandann = eegandann
        self.personality = personality
        self.detectedemotions = detectedemotions
        self.parametertable = parametertable

    def getwindow(self):
        return self.window
    def getheight(self):
        return self.height
    def getwidth(self):
        return self.width
    def getdatabaseurl(self):
        return self.databaseurl
    def getpassword(self):
        return self.password
    def geteegandann(self):
        return self.eegandann
    def getpersonality(self):
        return self.personality
    def getdetectedemotions(self):
        return self.detectedemotions
    def getparametertable(self):
        return self.parametertable

    def setwindow(self,window):
        self.window = window
    def setheight(self,height):
        self.height = height
    def setwidth(self,width):
        self.width = width
    def setdatabaseurl(self,databaseurl):
        self.databaseurl = databaseurl
    def setpassword(self,password):
        self.password = password
    def seteegandann(self, eegandann):
        self.eegandann = eegandann
    def setpersonality(self, personality):
        self.personality = personality
    def setdetectedemotions(self, detectedemotions):
        self.detectedemotions = detectedemotions
    def setparametertable(self, parametertable):
        self.parametertable = parametertable

    def makeWindow(self):

        self.getwindow().title("START")
        self.getwindow().geometry(str(self.getheight()) + "x" + str(self.getwidth()))

        # creating a button instance
        importdatabutton = Button(self.getwindow(), width=20, height=2, text="Import Data",command =lambda: self.importData())
        newdatabasebutton = Button(self.getwindow(), width=20, height=2, text="New Database",command =lambda: self.newDatabase())

        # placing the button on my window
        importdatabutton.place(x=75, y=20)
        newdatabasebutton.place(x=75,y=80)

        self.getwindow().mainloop()

    def newDatabase(self):

        self.getwindow().destroy()

        nowabaza = Confirmation(Tk(), self.getheight(), self.getwidth(), self.getdatabaseurl(), self.getpassword(),
                                     self.geteegandann(), self.getpersonality(), self.getdetectedemotions(),self.getparametertable())
        nowabaza.makeWindow()


    def importData(self):

        print("IMPROTING DATA")

        self.getwindow().destroy()

        imporujdane = ImportData(Tk(), self.getheight(), self.getwidth(), self.getdatabaseurl(), self.getpassword(),self.geteegandann(),self.getpersonality(),self.getdetectedemotions(),self.getparametertable())
        imporujdane.makeWindow()

