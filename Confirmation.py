from ImportData import *
from tkinter import *
from Start import *
from NewDatabase import *

class Confirmation:

    def __init__(self, window, height, width, databaseurl, password, eegandann, personality, detectedemotions,parametertable):
        self.window = window
        self.height = height
        self.width = width
        self.databaseurl = databaseurl
        self.password = password
        self.eegandann = eegandann
        self.personality = personality
        self.detectedemotions = detectedemotions
        self.parametertable = parametertable

    def getwindow(self):
        return self.window
    def getheight(self):
        return self.height
    def getwidth(self):
        return self.width
    def getdatabaseurl(self):
        return self.databaseurl
    def getpassword(self):
        return self.password
    def geteegandann(self):
        return self.eegandann
    def getpersonality(self):
        return self.personality
    def getdetectedemotions(self):
        return self.detectedemotions
    def getparametertable(self):
        return self.parametertable
    def setwindow(self, window):
        self.window = window
    def setheight(self, height):
        self.height = height
    def setwidth(self, width):
        self.width = width

    def setdatabaseurl(self, databaseurl):
        self.databaseurl = databaseurl
    def setpassword(self, password):
        self.password = password
    def seteegandann(self, eegandann):
        self.eegandann = eegandann
    def setpersonality(self, personality):
        self.personality = personality
    def setdetectedemotions(self, detectedemotions):
        self.detectedemotions = detectedemotions
    def setparametertable(self, parametertable):
        self.parametertable = parametertable

    def makeWindow(self):
        self.getwindow().title("CONFIRMATION")
        self.getwindow().geometry(str(self.getheight()) + "x" + str(self.getwidth()))

        Label(self.getwindow(), text="There already exists database on that machine, \n creating new will remove all existing data, \n are you sure to continue ?").place(x = 25,y  =50)

        # creating a button instance
        importdatabutton = Button(self.getwindow(), width=10, height=2, text="Exit", command= lambda: self.exit())
        newdatabasebutton = Button(self.getwindow(), width=10, height=2, text="Continue", command=lambda: self.newDatabase())

        # placing the button on my window
        importdatabutton.place(x=50, y=125)
        newdatabasebutton.place(x=175, y=125)

        self.getwindow().mainloop()

    def newDatabase(self):
        #print("MAKING NEW DATABASE")

        self.getwindow().destroy()

        nowabaza = NewDatabase(Tk(), self.getheight(), self.getwidth(), self.getdatabaseurl(), self.getpassword(),
                               self.geteegandann(), self.getpersonality(), self.getdetectedemotions(),
                              self.getparametertable())
        nowabaza.makeWindow()

    def exit(self):

        print("EXIING")

        self.getwindow().destroy()
