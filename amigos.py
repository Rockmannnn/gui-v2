from scipy.io import loadmat
import pandas as pd
import os
from data import *

def get_preprocessed(video_number=1, user_id=1):
    """Reads preprocessed data from a .mat file and returns is as a more accessible data structure.

    :param video_number:
    :param user_id:
    :return: dictionary with preprocessed data
    """

    # Load .mat file
    data_file = f"Data_Preprocessed_P{user_id:02}.mat"
    data_path = os.path.join(PATH_PREPROCESSED, data_file)
    print(f"Reading data from \"{data_path}\".")
    data_raw = loadmat(PATH_PREPROCESSED)

    data_joined = data_raw.get("joined_data")[0][video_number-1]
    data_ext_annotation = data_raw.get('labels_ext_annotation')[0][video_number-1]
    data_selfassessment = data_raw.get('labels_selfassessment')[0][video_number-1]

    data_dict = {
        "voltmat": pd.DataFrame(data_joined[:,0:14]),
        "ECG_right": data_joined[:, 14],
        "ECG_left": data_joined[:, 15],
        "video_id": data_raw.get("VideoIDs")[0][video_number - 1][0],
        "ext_annotation": data_ext_annotation,
        "selfassessment": data_selfassessment
    }

    # get timestamps from frequency and number of records
    time = pd.Timedelta(1/FREQUENCY*len(data_dict["ECG_right"]), 's')
    data_dict["time"] = time

    # name voltmat columns
    data_dict["voltmat"].columns = ELECTRODES

    return data_dict

def get_personality():
    # Load .xlsx file
    data_file = "Participants_Personality.xlsx"
    data_path = os.path.join(PATH_METADATA, data_file)
    print(f"Reading data from \"{data_path}\".")
    data_raw = pd.ExcelFile(PATH_METADATA)

    data_parsed = data_raw.parse("Personalities")
    return data_parsed