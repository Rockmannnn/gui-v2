import pandas as pd
import numpy as np
from window_slider import Slider
from scipy.stats import pearsonr
import itertools
from neo4j import GraphDatabase
import time

from data import *


def get_timestamps(driver, user_id, video_id):
    """
    Retrieves Time nodes values for specific User and Video.
    :param driver: neo4j bolt driver
    :param user_id: integer uniquely identifying a user
    :param video_id: string uniquely identifying a video
    :return: list of pd.TimeDelta
    """
    query = f"MATCH (:Video {{id:'{video_id}'}})-->(s)<--(:User {{id:{user_id}}}), "
    query += f"(s)-->(:MetaNode)-->(t:Time) RETURN t.value ORDER BY t.id;"

    results = []
    with driver.session() as session:
        for val in session.run(query).values(0):
            results.append(val[0])
    results = pd.to_timedelta(results)

    return results


def process_metanodes_time(driver, video_length, epoch_length, user_id, video_id):
    """
    Creates basic Metanodes (Video, User, Time). This method must be initialized before executing other as it creates
    the basic and necessary skeleton of the graph. Uses user and video ids to create User and Video nodes respectively
    and then assigns them to a Situation node.

    :param driver: neo4j bolt driver
    :param video_length: length of the video as pd.Timedelta
    :param epoch_length: length of an epoch in seconds
    :param user_id: integer uniquely identifying a user
    :param video_id: string uniquely identifying a video
    :return:
    """

    # change global variable EPOCH - all eeg graphs in current db must have the same epoch length
    global EPOCH, FREQUENCY
    EPOCH = epoch_length * FREQUENCY

    # FIXME:last timestamp is rounded up and longer than the video, e.g video: 65s, epoch: 20s, last ts: 80s (4 * epoch)
    timestamps = pd.timedelta_range(start=pd.Timedelta(0, 's'),
                                    end=video_length,
                                    freq=str(epoch_length) + 's')

    # retrieve number of Time nodes from generated timestamps
    num_epoch = len(timestamps)
    print(f"Inserting Time nodes with timestamps: {list(timestamps)}")

    query = f"MERGE (situation: Situation{{type:'Watching a movie', user_id:{user_id}, video_id:{video_id}}})\n"
    query += f"MERGE (video: Video {{id: '{video_id}'}})\n"
    query += f"MERGE (video)-[:TOOK_PART_IN]->(situation)\n"
    query += f"MERGE (user: User {{id: {user_id}}})\n"
    query += f"MERGE (user)-[:TOOK_PART_IN]->(situation)\n"
    query += f"MERGE (situation)-[:HAS]->(data_time: MetaNode {{name: 'Time'}})\n"

    for signal_id in range(num_epoch):
        query += f"MERGE (data_time)-[:HAS]->(t{signal_id+1}: Time {{id: {signal_id+1}, value: '{timestamps[signal_id]}'}})\n"

    n1 = 1
    n2 = 2
    while n2 <= num_epoch:
        query += f"MERGE (t{n1})-[:NEXT]->(t{n2})\n"
        query += f"MERGE (t{n1})<-[:PREVIOUS]-(t{n2})\n"
        n1 += 1
        n2 += 1

    session = driver.session()
    session.run(query)
    session.close()


# TODO: Optimize, optimize, optimize! Maybe save data as .csv and bulk import to neo4j?
def process_voltmat(driver, data, electrodes, user_id, video_id):
    """Sends data to :func:`__roll_electrode` for processing and creates a graph in a neo4j database using
    :func:`__process_electrode`.

    :param driver: neo4j bolt driver
    :param data: pandas DataFrame with voltmat readings with constant frequency, columns correspond to electrodes
    :param electrodes: list of electrode names, must be equal to the number of data columns
    :param user_id: integer uniquely identifying a user
    :param video_id: string uniquely identifying a video
    :return: time taken in seconds
    """
    print(f"Processing voltmat with electrodes: {electrodes}...")
    time_start = time.time()

    # number of electrodes must match number of DataFrame columns
    electrodes_num = len(electrodes)
    if data.shape[1] != electrodes_num:
        raise(ValueError("Electrodes array length should be equal to the number of DataFrame columns!"))

    # cycle through electrodes
    for electrode_i in range(0, electrodes_num):
        electrode_name = electrodes[electrode_i]
        print(f"Processing electrode: {electrode_name}...")
        data_one_electrode = np.array(data.iloc[:, electrode_i])
        # divide into epochs and sub segments
        data_rolled, data_corr = __roll_electrode(driver=driver, data=data_one_electrode, electrode=electrode_name, video_id=video_id, user_id=user_id)
        # import into neo4j
        __process_electrode(data_rolled=data_rolled, data_corr=data_corr, electrode=electrode_name, user_id=user_id,
                            video_id=video_id, driver=driver)

    time_end = time.time()
    time_taken = time_end-time_start
    print(f"Time taken: {round(time_taken, 2)}s")
    return time_taken


def __roll_electrode(driver, data, electrode, user_id, video_id):
    """Divides readings into epochs and then into sub segments using the sliding window method and returns DataFrame.
    Uses :func:`__get_corr` to calculate correlation between sub segments within epoch.

    :param driver: neo4j bolt driver
    :param data: array to be divided
    :param electrode: name of the electrode
    :return: two DataFrames, data_rolled with data divided into epochs and sub segments, data_corr with correlations between sub segments
    """

    # TODO: make sure this issue is really, >really< fixed.
    # if (EPOCH - WINDOW) % (WINDOW - OVERLAP) != 0:
    #     raise(ValueError("Picked window, overlap and epoch values will result in lost data!"))

    # Calculate the number of epochs.
    timestamps = get_timestamps(driver=driver, user_id=user_id, video_id=video_id)

    number_of_epochs = len(timestamps)

    data_corr = pd.DataFrame(columns=["electrode", "epoch_id", "index_a", "corr", "index_b"])
    data_rolled = pd.DataFrame(columns=["epoch_id", "index", "subsegment"])

    for epoch_num in range(0, number_of_epochs):
        epoch_start = round(timestamps[epoch_num].total_seconds() * FREQUENCY)
        if epoch_num+1 >= number_of_epochs:
            epoch_end = len(data)
        else:
            epoch_end = round(timestamps[epoch_num+1].total_seconds() * FREQUENCY)

        # If the last epoch is too short to fit the rest of the data, make it longer.
        # if epoch_num+1 == number_of_epochs and epoch_end < len(data):
        #     epoch_end = len(data)

        data_epoch = data[epoch_start:epoch_end]

        # Divide array using sliding window with overlap
        slider = Slider(WINDOW, OVERLAP)
        slider.fit(data_epoch)
        data_one_epoch = pd.DataFrame(columns=["epoch_id", "index", "subsegment"])

        segment_index = 0
        while True:
            new_subsegment = slider.slide()
            new_subsegment_len = len(new_subsegment)

            # Only add sub segment if it has new data (isn't just overlap)
            if new_subsegment_len > OVERLAP:

                # If sub segment is too short lengthen the overlap with previous sub segment to fit length.
                if new_subsegment_len < WINDOW:
                    # TODO: simplify code because it's kinda trash (could be probably done without a for loop)
                    for i in range(0, WINDOW - new_subsegment_len):
                        new_subsegment = np.insert(new_subsegment, 0, data_one_epoch.iloc[-1]["subsegment"][-i-1-OVERLAP])

                # Add new subsegment to DataFrame
                data_one_epoch = data_one_epoch.append({"epoch_id": epoch_num+1,
                                                        "index": segment_index,
                                                        "subsegment": new_subsegment}, ignore_index=True)
                segment_index += 1

            if slider.reached_end_of_list():
                break

        # Get correlation between subsegments within epoch
        data_corr = data_corr.append(__get_corr(data_one_epoch, electrode), ignore_index=True)
        data_rolled = data_rolled.append(data_one_epoch, ignore_index=True)

    return data_rolled, data_corr


def __get_corr(data_rolled, electrode):
    """ Calculates correlation for all possible pairs of sub segments within a single epoch and returns them as a pandas
    Dataframe.
    :param data_rolled: DataFrame with a single epoch divided into sub segments as returned by :func:`__roll_electrode`
    :param electrode: name of the electrode
    :return: DataFrame with correlation between all sub segment pairs within epoch
    """
    data_corr = pd.DataFrame(columns=["electrode", "epoch_id", "index_a", "corr", "index_b"])
    epoch_id = data_rolled.iloc[0]["epoch_id"]

    # Get all possible combinations of subsegments ...
    cc = list(itertools.combinations(range(0, data_rolled.shape[0]), 2))
    for index_a, index_b in cc:
        # ... and calculate their correlation
        subsegment_a = data_rolled.iloc[index_a]["subsegment"]
        subsegment_b = data_rolled.iloc[index_b]["subsegment"]
        data_corr = data_corr.append({"electrode": electrode,
                                      "epoch_id": epoch_id,
                                      "index_a": index_a,
                                      "corr": pearsonr(subsegment_a, subsegment_b)[0],
                                      "index_b": index_b}, ignore_index=True)

    return data_corr


def __process_electrode(driver, data_rolled, data_corr, electrode, user_id, video_id):
    """Imports an EEG graph to a neo4j database.

    :param driver: neo4j bolt driver
    :param data_rolled: DataFrame as returned by :func:`__roll_electrode`
    :param data_corr: DataFrame as returned by :func:`__roll_electrode`
    :param electrode: name of the electrode
    :param user_id: Integer uniquely identifying a user
    :param video_id: string uniquely identifying a video
    :return:
    """

    print(f"Inserting electrode {electrode} into a neo4j database...")
    epoch_num = data_corr['epoch_id'].max()

    # if not exists: create User node and Data nodes for eeg and electrode and their HAS relationships
    query = f"MERGE (situation: Situation {{type:'Watching a movie', user_id:{user_id}, video_id:{video_id}}})\n" \
            f"MERGE (situation)-[:HAS]->(data_eeg: MetaNode {{name: 'EEG'}})\n" \
            f"MERGE (data_eeg)-[:HAS]->(data_electrode: MetaNode {{name: '{electrode}'}})\n" \
            f"MERGE (situation)-[:HAS]->(data_time: MetaNode {{name: 'Time'}})\n"

    # TODO: add actual timestamps instead of epoch numbers? might require passing frequency as argument (see first TODO)
    # create all epoch Root nodes and Time nodes
    query += f"MERGE (data_time)-[:HAS]->(time_1:Time {{id:1}})\n"
    for epoch_id in range(1, epoch_num + 1):
        query += f"CREATE (data_electrode)-[:HAS]->(root_{epoch_id}:Root)<-[:HAS]-(time_{epoch_id})\n"
        if (epoch_id < epoch_num):
            query += f"MERGE (data_time)-[:HAS]->(time_{epoch_id+1}:Time {{id:{epoch_id+1}}})\n"
            query += f"MERGE (time_{epoch_id+1})<-[:NEXT]-(time_{epoch_id})\n"
            query += f"MERGE (time_{epoch_id+1})-[:PREVIOUS]->(time_{epoch_id})\n"

    # create all Signal nodes
    i = 0
    for _, row in data_rolled.iterrows():
        i+=1
        epoch_id = row['epoch_id']
        subsegment_index = row['index']

        # TODO: insert actual values? maybe? cause this seems kinda important, ya know? (paper specified 12 values)
        query += f"CREATE (signal_{epoch_id}_{subsegment_index}: Signal {{value: '', id:{i}}})<-[:HAS]-(root_{epoch_id})\n"

    # connect Signal values which pass correlation threshold
    for _, row in data_corr.iterrows():
        epoch_id = row['epoch_id']
        index_a = row['index_a']
        index_b = row['index_b']
        corr = row['corr']
        if corr > THRESHOLD:
            query += f"CREATE (signal_{epoch_id}_{index_a})-[:CORR {{value: {corr}}}]->(signal_{epoch_id}_{index_b})\n"

    session = driver.session()
    session.run(query)
    session.close()


def create_personality_node(driver, data, user_id):
    session = driver.session()

    print(f"Inserting personality for user {user_id}...")

    #query = "CREATE (n: Personality {UserID: '"+str(user_id)+"', Extrovertion: '"+str(df[user_id][0])+"', Agreeableness:'"+str(df[user_id][1])+"', Conscientiousness:'"+str(df[user_id][2])+"', Emotional_Stability: '"+str(df[user_id][3])+"', Creativity: '"+str(df[user_id][4])+"'})"

    query = "MATCH (a:User {id: "+str(user_id)+"}) MERGE (:Personality{UserID: '"+str(user_id)+"', Extroversion: '"+str(data[user_id][0])+"', Agreeableness:'"+str(data[user_id][1])+"', Conscientiousness:'"+str(data[user_id][2])+"', Emotional_Stability: '"+str(data[user_id][3])+"', Creativity: '"+str(data[user_id][4])+"'})<-[r:HAS]-(a)"
    session.run(query)

    session.close()


# Running as main tests processing time on small dataset.
# This will WIPE / MESS WITH YOUR DATABASE!!!
if __name__ == "__main__":
    pd.set_option('display.max_columns', 10)

    EPOCH = 16
    WINDOW = 3
    OVERLAP = 1
    NUM_TESTS = 5
    TEST_SIZE = 128
    DELETE_DATA = False

    print(f"Data set size: {TEST_SIZE}")
    print(f"Epochs: {EPOCH}")
    print(f"Window: {WINDOW}")
    print(f"Overlap: {OVERLAP}")
    print(f"Number of tests: {NUM_TESTS}")
    confirm = None
    while confirm is None:
        response = input("Run test? (y/n): ")
        if response == "y":
            if not DELETE_DATA:
                confirm = True
            while DELETE_DATA and confirm is None:
                response = input("This will WIPE YOUR DATABASE! Are you sure? (y/n): ")
                if response == "y":
                    confirm = True
                elif response == "n":
                    confirm = False
        elif response == "n":
            confirm = False

    if confirm:
        electrodes = ["AF3", "F3"]
        times = []
        for test_id in range(0, NUM_TESTS):
            if DELETE_DATA:
                driver = GraphDatabase.driver(databaseurl, auth=("neo4j", neo4j_pass), encrypted=False)
                session = driver.session()
                session.run("MATCH (n) DETACH DELETE n")

            data_test = np.random.normal(loc=7, scale=3.5, size=TEST_SIZE)
            data_test = pd.DataFrame({"AF3": data_test, "F3": data_test + 16})

            print(f"\nTEST {test_id+1}")
            times.append(process_voltmat(data_test, electrodes, test_id+1))

        if DELETE_DATA:
            driver = GraphDatabase.driver(databaseurl, auth=("neo4j", neo4j_pass), encrypted=False)
            session = driver.session()
            session.run("MATCH (n) DETACH DELETE n")

        print(f"Average time: {np.mean(times)}s")

